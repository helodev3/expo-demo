import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import * as FileSystem from 'expo-file-system';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {deleteAsync, getFreeDiskStorageAsync, getInfoAsync, getTotalDiskCapacityAsync} from "expo-file-system";
import {TouchableOpacity} from "react-native";
import { Audio } from 'expo-av';
import {Alert} from "react-native";

export default class HomeScreen extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            downloadProgress:0,
            freeMemory:0,
            totalMemory:0,
            fileExistResult:'',
            sound:undefined
        }
    }

    _callback = downloadProgress => {
        const progress = downloadProgress.totalBytesWritten / downloadProgress.totalBytesExpectedToWrite;
        this.setState({
            downloadProgress: progress,
        });
    };

    _downloadResumable = FileSystem.createDownloadResumable(
        'http://localhost/music.mp3',
        FileSystem.documentDirectory + 'music.mp4',
        {},
        this._callback
    );
    _startDownload = async () => {
        try {
            const { uri } = await this._downloadResumable.downloadAsync();
            console.log('Finished downloading to ', uri);
            AsyncStorage.removeItem('pauseDownload')

        } catch (e) {
            console.error(e);
        }
    }
    _pauseDownload = async () => {
        try {
            await this._downloadResumable.pauseAsync();
            console.log('Paused download operation, saving for future retrieval');
            AsyncStorage.setItem('pausedDownload', JSON.stringify(this._downloadResumable.savable()));
        } catch (e) {
            console.error(e);
        }
    }
    _resumeDownload = async () => {
        try {
            const { uri } = await this._downloadResumable.resumeAsync();
            console.log('Finished downloading to ', uri);
            AsyncStorage.removeItem('pauseDownload')
        } catch (e) {
            console.error(e);
        }
    }

    _resumeDownaloadAfterAppRestart = async () =>{
        //To resume a download across app restarts, assuming the the DownloadResumable.savable() object was stored:
        // First check if the file exists on async storage
        const downloadSnapshotJson = await AsyncStorage.getItem('pausedDownload');
        const downloadSnapshot = JSON.parse(downloadSnapshotJson);
        const downloadResumable = new FileSystem.DownloadResumable(
            downloadSnapshot.url,
            downloadSnapshot.fileUri,
            downloadSnapshot.options,
            callback,
            downloadSnapshot.resumeData
        );

        try {
            const { uri } = await downloadResumable.resumeAsync();
            console.log('Finished downloading to ', uri);
            AsyncStorage.removeItem('pauseDownload')

        } catch (e) {
            console.error(e);
        }
    }

    _deleteDownloadedFile = async () => {
        await deleteAsync(FileSystem.documentDirectory + 'music.mp4')
    }

    componentDidMount = async () => {
        const freeMemory = await getFreeDiskStorageAsync()
        const totalMemory = await getTotalDiskCapacityAsync()
        this.setState({
            freeMemory,
            totalMemory
        })
    }

    downloadSimulation = () => {
        this._startDownload()
    }
    checkFileExist = () => {
        Alert.alert(FileSystem.documentDirectory + 'music.mp4')
        getInfoAsync(FileSystem.documentDirectory + 'music.mp4')
            .then(r=>{
                this.setState({
                    fileExistResult:r.exists.toString()
                })
            })
    }
    deleteFile = () => {
        deleteAsync(FileSystem.documentDirectory + 'music.mp4')

    }

    playAudio = () => {

    }
    _sound = undefined
    async  playSound() {
        console.log('Loading Sound');
        FileSystem.getContentUriAsync(FileSystem.documentDirectory + 'music.mp4')
            .then(r=>{
                const { sound } = Audio.Sound.createAsync(
                    {uri:r}
                );
                this.setState({
                    sound
                })

                console.log('Playing Sound');
                sound.playAsync();
            })


    }



    render() {
        const progressStyle = {position:'absolute',
            height:30,
            backgroundColor: 'red',
            width:100*this.state.downloadProgress.toString()+'%'}
        return(
            <View>
                <Text>
                    Available FS mem is {Math.round(100*this.state.freeMemory/this.state.totalMemory)} %
                </Text>
                <Text>
                    File exist result {this.state.fileExistResult}
                </Text>
                <View>
                    <TouchableOpacity
                        onPress={this.downloadSimulation}
                        style={{backgroundColor: 'blue',marginTop:25}}>
                        <Text style={{textAlign:'center',color:'white'}}>Simulate Download</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.checkFileExist}
                        style={{backgroundColor: 'blue',marginTop:25}}>
                        <Text style={{textAlign:'center',color:'white'}}>Check File Exist</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.deleteFile}
                        style={{backgroundColor: 'blue',marginTop:25}}>
                        <Text style={{textAlign:'center',color:'white'}}>Delete File Exist</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.playSound}
                        style={{backgroundColor: 'blue',marginTop:25}}>
                        <Text style={{textAlign:'center',color:'white'}}>Play audio</Text>
                    </TouchableOpacity>

                </View>
                <View style={{width: '50%', backgroundColor:'blue',marginTop:20}}>
                    <View style={progressStyle}>
                        <Text>.</Text>
                    </View>
                    <Text style={{color:'black',position: 'absolute'}}>
                        Download progress is at {100*this.state.downloadProgress} %
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
